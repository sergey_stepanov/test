<?php
/**
 * Created by PhpStorm.
 * User: s.stepanov
 * Date: 09.03.2017
 * Time: 21:07
 */

namespace task1;

/**
 * Class init
 * @package task1
 */
final class init
{
    /**
     * @var \PDO
     */
    protected $db;

    /**
     * init constructor.
     * @param $_connect
     */
	public function __construct($_connect)
    {
        $this->db = $_connect;
        $this->create();
        $this->fill();
    }

    /**
     * @description Создает таблицу в БД, если та не существует
     * @return bool|string
     */
    private function create()
    {
        try {
            $this->db->beginTransaction();
            $this->db->exec(
                'CREATE TABLE IF NOT EXISTS `test` (
                `id` INTEGER(5) UNSIGNED AUTO_INCREMENT,
                `script_name` VARCHAR(25),
                `start_time` INTEGER(10),
                `end_time` INTEGER(10),
                `result` ENUM(\'normal\', \'illegal\', \'failed\', \'success\'),
                PRIMARY KEY (id))
                ENGINE MyIsam CHARACTER SET utf8'
            );
            $this->db->commit();
            return true;
        }
        catch (\Exception $e) {
            $this->db->rollBack();
            echo 'Error: '.$e->getMessage();
        }
    }

    /**
     * @param int $_rows
     * @description Заполняет таблицу test тестовыми данными
     * @return bool|string
     */
    private function fill($_rows = 10)
    {
        try {
            $stmt = $this->db->prepare(
                'INSERT INTO `test`
                  (
                    `script_name`,
                    `start_time`,
                    `end_time`,
                    `result`
                  )
                  VALUES
                  (
                    :script_name,
                    :start_time,
                    :end_time,
                    :res
                  )'
            );
            $stmt->bindParam(':script_name', $name);
            $stmt->bindParam(':start_time', $start);
            $stmt->bindParam(':end_time', $end);
            $stmt->bindParam(':res', $res);

            for ($i = 0; $i <= $_rows; $i++) {
                $name = $this->generateName();
                $start = $this->generateTime();
                $end = $this->generateTime();
                $res = mt_rand(1, 4);
                $stmt->execute();
            }
            return true;
        }
        catch (\Exception $e) {
            $this->db->rollBack();
            echo 'Error: '.$e->getMessage();
        }
    }

    /**
     * @return int
     * @description негерирует случайный timestamp
     */
    private function generateTime()
    {
        return time(0,0,0,rand(2005,2105),rand(1,31),rand(1,12));
    }

    /**
     * @param int $_length
     * @return string
     * @description генерирует случайную строку длинной $length
     */
    private function generateName($_length = 15)
    {
        $characters = 'abcdefghiklmnopqastuvwxyzABCDEFGHIJKLMNOPQRSTUVWQYZ';
        $name = '';

        for ($i = 0; $i <= $_length; $i++)
            $name .= $characters[mt_rand(0, strlen($characters)-1)];

        return $name;
    }

    /**
     * @return array|bool
     * @description получает значения из таблицы test по полю result
     */
    public function get()
    {
        try {
            $stmt = $this->db->prepare(
                'SELECT *
                FROM `test`
                WHERE `result` = \'normal\' 
                   OR `result` = \'success\'');

            if ($stmt->execute()) {
                $result = [];
                while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                    // echo '<pre>'; print_r($row); echo '</pre>';
                    $result[] = $row;
                }
                return $result;
            }
        }
        catch(\PDOException $e){
            echo 'PDO error: '.$e->getMessage();
        }
        catch (\Exception $e) {
            echo 'Error: '.$e->getMessage();
        }
    }
}