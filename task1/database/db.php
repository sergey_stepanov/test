<?php
/**
 * Created by PhpStorm.
 * User: s.stepanov
 * Date: 09.03.2017
 * Time: 21:05
 */

namespace task1\database;

/**
 * Class db
 * @package task1\database
 */
class db
{
    /**
     * @var \PDO
     */
    protected static $connection;

    /**
     * db constructor.
     * @param $_data
     * @throws \Exception
     */
    private function __construct($_data)
    {
        try {
            self::$connection = new \PDO(
                $_data['db_type'].':host='.$_data['server'].';dbname='.$_data['db_name'],
                $_data['username'],
                $_data['password'],
                $_data['option']
            );
        }
        catch (\PDOException $e) {
            throw $e;
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    private function __clone()
    {
    }
    private function __wakeup()
    {
    }

    /**
     * @param $_data
     * @return \PDO
     */
    final public static function instance($_data)
    {
        if (self::$connection === null) {
            new self($_data);
        }

        return self::$connection;
    }


}