<?php
/**
 * Created by PhpStorm.
 * User: s.stepanov
 * Date: 09.03.2017
 * Time: 21:05
 */

$data = [
    'db_type' => 'mysql',
    'db_name' => 'test_db',
    'server' => 'localhost',
    'username' => 'root',
    'password' => '',
    'option' =>[PDO::ATTR_CASE => PDO::CASE_NATURAL]
];