<?php
/**
 * Created by PhpStorm.
 * User: s.stepanov
 * Date: 09.03.2017
 * Time: 21:01
 */

require_once './database/config.php';
require_once './autoload.php';

$connection = \task1\database\db::instance($data);
$init = new \task1\init($connection);
$res = $init->get();
// echo '<pre>'; print_r($res); echo '</pre>';
