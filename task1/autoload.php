<?php
/**
 * Created by PhpStorm.
 * User: s.stepanov
 * Date: 09.03.2017
 * Time: 21:02
 */

/**
 * @description Автозагрузка
 */
spl_autoload_register(function($className){
    $className = ltrim($className, '\\');

    $fileName = __DIR__. DIRECTORY_SEPARATOR ;
    $fileNameArray = explode('\\', $className);

    if (reset($fileNameArray) == 'task1')
        unset($fileNameArray[key($fileNameArray)]);

    $fileNameString = implode(DIRECTORY_SEPARATOR, $fileNameArray);
    $fileName .= $fileNameString.'.php';

    if (file_exists($fileName))
        require_once $fileName;
    else
        echo "file {$fileName} not exist";
}, true, true);