<?php
/**
 * Created by PhpStorm.
 * User: doctor
 * Date: 09.03.2017
 * Time: 22:17
 */

namespace task3;

/**
 * Class fileInspector
 * @package task3
 */
class fileInspector
{
    protected $pattern = '/^[a-zA-Z\d]+\.ixt$/i';
    protected $fileList = [];

    /**
     * @param string $directory
     */
    public function __construct($directory = 'datafiles')
    {
        $dir = $this->setDirectory($directory);

        $this->getFiles($dir);
        $this->sortResult();
        $this->viewResult();
    }

    /**
     * @param $directory
     * @description Устанавливает директорию для поиска.
     * @return string|bool
     */
    private function setDirectory($directory)
    {
        $path = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."task3".DIRECTORY_SEPARATOR.trim($directory).DIRECTORY_SEPARATOR;
        if (is_dir($path))
            return $path;

        return false;
    }

    /**
     * @param string $_dir
     * @throws \Exception
     * @description Метод рекурсивно сканирует указанную директорию и генерирует список файлов в соответсвии с
     * критерием $this->pattern.
     */
    private function getFiles($_dir)
    {
        if (!is_dir($_dir))
            throw new \Exception("Путь не является директорией");

        $tempResult = scandir($_dir);
        if (false === $tempResult)
            throw new \Exception('Ошибка при сканировании директории');

        foreach ($tempResult as $item) {
            if ('.' == $item || '..' == $item)
                continue;

            if (!is_dir($_dir.DIRECTORY_SEPARATOR.$item) && preg_match($this->pattern, $item))
                $this->fileList[] = $item;
            elseif (is_dir($_dir.DIRECTORY_SEPARATOR.$item))
                $this->getFiles($_dir.DIRECTORY_SEPARATOR.$item);
        }

    }

    /**
     * @param string $_sort
     * @description Произвоит сортировку списка файлов
     * @return void
     */
    private function sortResult($_sort = 'asc')
    {
        if ('asc' == $_sort)
           asort($this->fileList);
        elseif ('desc' == $_sort)
            arsort($this->fileList);
    }

    /**
     * @description Произвоит отображение списка файлов на экран
     * @return void
     */
    private function viewResult()
    {
        echo '<pre>'; print_r($this->fileList); echo '</pre>';
    }
}